# AKS configuration and deployment :fire:

## Co potrzebne?
Musimy miec na lokalnym środowisku zainstalowane:
- terraform
- az cli
- kubectl
- docker

## Instalacja AKS oraz konfiguracja GitLab Agenta
0. Zalogować sie do Azure:
```
az login
```
1. Postawienie infrastruktury (klastra kubernetesa, grupy zasobów):
```
cd iaac
terraform apply
cd ..
```

2. Połączenie się z klastrem:
```
az aks get-credentials --resource-group k8s-dashboard-group --name k8sDashboard
```

3. Wpięcie GitLab Agenta do klastra, aby móc deployowac aplikacje:
```
docker run --pull=always --rm \
    registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable generate \
    --agent-token=$TOKEN \
    --kas-address=wss://kas.gitlab.com \
    --agent-version stable \
    --namespace gitlab-kubernetes-agent | kubectl apply -f -
```

3. Teraz możemy uruchomić pipeline na Gitlabie, który zbuduje wszystkie obrazy dokerowe.

4. Kiedy deploy dobiegnie końca, będąc jeszce w kontekście możemy pozyskać adres publicznego ip:
```
kubectl get svc
```
na liście będzie nasz LoadBalancer, z którego interesuje nas pole EXTERNAL-IP.


5. Usunięcie całej infrastruktury:
```
cd iaac
terraform destroy
cd ..
```

## Metryki

Pod adresem `/metrics` znajduja się wszystkie metryki dot. aktualnych requestów przychodzących do systemu jak i statusów z jakimi są zakończane. Metryki są zbierane za pomocą Prometheus'a.

## Healthcheck

Pod adresem `/health` dostępny jest endpoint sprawdzający czy dany serwis działa poprawnie. Sprawdzane są tam połączenia do bazy danych czy do Rabbita.

## Logi

Jako system logów, wykożystywane jest narzędzie Grafana Loki. Stdout z naszej aplikacji przekierowywany jest właśnie do tego miejsca, gdzie wszystkie logi są zbierane. Potem możemy je przegladać w Grafanie.