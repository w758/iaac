import {CommandHandler, EventBus, ICommandHandler} from '@nestjs/cqrs';
import {CreateDeviceCommand} from '../commands/create-device.command';
import {Inject, LoggerService} from '@nestjs/common';
import {DeviceRepositoryInterface} from '../../Domain/device/device.repository.interface';
import {DeviceFactory} from '../factories/device.factory';
import {LocationRepositoryInterface} from '../../Domain/location/location.repository.interface';
import {LocationNotFoundException} from '../../Domain/location/exceptions/location-not-found.exception';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino';

@CommandHandler(CreateDeviceCommand)
export class CreateDeviceHandler implements ICommandHandler<CreateDeviceCommand> {
    constructor(
        private eventBus: EventBus,
        @Inject('DeviceRepositoryInterface') private readonly deviceRepository: DeviceRepositoryInterface,
        @InjectPinoLogger(CreateDeviceHandler.name) private readonly logger: PinoLogger,
        @Inject('LocationRepositoryInterface') private readonly locationRepository: LocationRepositoryInterface,
    ) {}

    async execute(command: CreateDeviceCommand): Promise<any> {
        this.logger.debug(`Handle command: ${JSON.stringify(command)})`)

        const location = await this.locationRepository.findById(command.locationId)

        if (location) {
            const device = DeviceFactory.create(
                command.url,
                command.name,
                command.type,
                location
            )

            await this.deviceRepository.save(device)

            return device
        } else {
            throw LocationNotFoundException.withId(command.locationId)
        }
    }
}

