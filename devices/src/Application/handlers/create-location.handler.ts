import {CommandHandler, EventBus, ICommandHandler} from '@nestjs/cqrs';
import {Inject, LoggerService} from '@nestjs/common';
import {CreateLocationCommand} from '../commands/create-location.command';
import {LocationFactory} from '../factories/location.factory';
import {LocationRepositoryInterface} from '../../Domain/location/location.repository.interface';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino';

@CommandHandler(CreateLocationCommand)
export class CreateLocationHandler implements ICommandHandler<CreateLocationCommand> {
    constructor(
        private eventBus: EventBus,
        @InjectPinoLogger(CreateLocationHandler.name) private readonly logger: PinoLogger,
        @Inject('LocationRepositoryInterface') private readonly locationRepository: LocationRepositoryInterface,
    ) {}

    async execute(command: CreateLocationCommand): Promise<any> {
        this.logger.debug(`Handle command: ${JSON.stringify(command)})`, 'LocationCreateCommand')

        const location = LocationFactory.create(
            command.name,
            command.userId
        )

        await this.locationRepository.save(location)

        return location
    }
}
