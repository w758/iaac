import {Device} from '../../Domain/device/device.entity';
import {Type} from '../../Domain/device/type';
import {slugify} from '../generators/slug.generator';
import {Location} from '../../Domain/location/location.entity';

export class DeviceFactory {
    static create(
        url: string,
        name: string,
        type: Type,
        location: Location,
    ): Device {
        return Device.create(
            url, name, type, slugify(name), location
        )
    }
}
