import {slugify} from '../generators/slug.generator';
import {Location} from '../../Domain/location/location.entity';

export class LocationFactory {
    static create(
        name: string,
        userId: string
    ): Location {
        return Location.create(
            name, userId, slugify(name)
        )
    }
}
