import {Device} from '../../Domain/device/device.entity';

export interface DeviceListQuery {
    devices(): Promise<Device[]>
}
