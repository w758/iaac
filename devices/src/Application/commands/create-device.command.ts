import {Type} from '../../Domain/device/type';

export class CreateDeviceCommand {
    constructor(
        public readonly name: string,
        public readonly url: string,
        public readonly type: Type,
        public readonly locationId: string,
        public readonly historyLogging: boolean = false
    ) {}
}
