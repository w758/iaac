import {IsBoolean, IsEnum, IsOptional, IsString, IsUrl, IsUUID} from 'class-validator';
import {Type} from '../../Domain/device/type';

export class DeviceDto {
    @IsString()
    public readonly name: string

    @IsUrl()
    public readonly url: string

    @IsEnum(Type)
    public readonly type: Type

    @IsUUID(4)
    public readonly locationId: string

    @IsBoolean()
    @IsOptional()
    public readonly historyLogging: boolean
}
