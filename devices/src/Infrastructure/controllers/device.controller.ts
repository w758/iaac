import {Controller, Get, HttpStatus, Res, Body, Post, Inject, Param} from '@nestjs/common';
import {Response} from 'express'
import {DeviceDto} from '../../Application/dtos/device.dto';
import {CommandBus} from '@nestjs/cqrs';
import {CreateDeviceCommand} from '../../Application/commands/create-device.command';
import {DeviceListQuery} from '../../Application/queries/device-list.query';
import {InjectPinoLogger, PinoLogger} from 'nestjs-pino';
import {DeviceRepositoryInterface} from '../../Domain/device/device.repository.interface';

@Controller('/devices')
export class DeviceController {
  constructor(
    private readonly commandBus: CommandBus,
    @Inject('DeviceListQuery') private readonly deviceList: DeviceListQuery,
    @Inject('DeviceRepositoryInterface') private readonly deviceRepository: DeviceRepositoryInterface,
    @InjectPinoLogger(DeviceController.name) private readonly logger: PinoLogger,
  ) {}

  @Get()
  async listAction(
      @Res() res: Response
  ): Promise<any> {
    try {
      return res.status(200).send(await this.deviceList.devices())
    } catch (e) {
      this.logger.error(e)

      return res.status(HttpStatus.BAD_REQUEST).json({
        error: {
          message: e.message,
        },
      });
    }
  }

  @Get('/:id')
  async getAction(
      @Res() res: Response,
      @Param('id') id: string
  ): Promise<any> {
    try {
      return res.status(200).send(await this.deviceRepository.findById(id))
    } catch (e) {
      this.logger.error(e)

      return res.status(HttpStatus.BAD_REQUEST).json({
        error: {
          message: e.message,
        },
      });
    }
  }

  @Post()
  async createAction(
      @Res() res: Response,
      @Body() deviceDto: DeviceDto
  ): Promise<any> {
    try {
      const device = await this.commandBus.execute(
          new CreateDeviceCommand(deviceDto.name, deviceDto.url, deviceDto.type, deviceDto.locationId, deviceDto.historyLogging)
      )

      return res.status(201).send(device)
    } catch (e) {
      this.logger.error(e)

      return res.status(HttpStatus.BAD_REQUEST).json({
        error: {
          message: e.message,
        },
      });
    }
  }
}
