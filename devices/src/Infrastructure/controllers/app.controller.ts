import {Controller, Get, HttpStatus, Res} from '@nestjs/common';
import {Response} from 'express'

@Controller()
export class AppController {
  @Get()
  mainAction(
      @Res() res: Response
  ): any {
    return res.status(HttpStatus.NO_CONTENT).send()
  }
}
