import {Controller, Get, HttpStatus, Res, Body, Post, Inject, Query} from '@nestjs/common';
import {Response} from 'express'
import {CommandBus} from '@nestjs/cqrs';
import {LocationDto} from '../../Application/dtos/location.dto';
import {CreateLocationCommand} from '../../Application/commands/create-location.command';
import {LocationListQuery} from '../../Application/queries/location-list.query';
import {LocationsFilterOptionsDto} from '../../Application/dtos/locations-filter-options.dto';

@Controller('/locations')
export class LocationController {
  constructor(
      private readonly commandBus: CommandBus,
      @Inject('LocationListQuery') private readonly locationList: LocationListQuery
  ) {}

  @Get()
  async listAction(
      @Res() res: Response,
      @Query() filterOptions: LocationsFilterOptionsDto
  ): Promise<any> {
    try {
      return res.status(200).send(await this.locationList.locations(filterOptions))
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: {
          message: e.message,
        },
      });
    }
  }

  @Post()
  async createAction(
      @Res() res: Response,
      @Body() locationDto: LocationDto
  ): Promise<any> {
    try {
      const location = await this.commandBus.execute(
          new CreateLocationCommand(locationDto.name, locationDto.userId)
      )

      return res.status(201).send(location)
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: {
          message: e.message,
        },
      });
    }
  }
}
