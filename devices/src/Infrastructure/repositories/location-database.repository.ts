import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {LocationRepositoryInterface} from '../../Domain/location/location.repository.interface';
import {Location} from '../../Domain/location/location.entity';
import {LocationListQuery} from '../../Application/queries/location-list.query';
import {LocationsFilterOptionsDto} from '../../Application/dtos/locations-filter-options.dto';

export class LocationDatabaseRepository implements LocationRepositoryInterface, LocationListQuery {
    constructor(
        @InjectRepository(Location) private readonly repository: Repository<Location>
    ) {}

    async save(location: Location): Promise<void> {
        await this.repository.save(location)
    }

    async findById(locationId: string): Promise<Location|undefined> {
        return await this.repository.findOne(locationId)
    }

    async locations(filterOptions: LocationsFilterOptionsDto): Promise<Location[]> {
        let where = {}

        if (filterOptions.userId) {
            where = Object.assign(where, {userId: filterOptions.userId})
        }

        return await this.repository.find({
            relations: ['devices'],
            where
        })
    }
}
