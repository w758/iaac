import {InjectRepository} from '@nestjs/typeorm';
import {Device} from '../../Domain/device/device.entity';
import {Repository} from 'typeorm';
import {DeviceRepositoryInterface} from '../../Domain/device/device.repository.interface';
import {DeviceListQuery} from '../../Application/queries/device-list.query';

export class DeviceDatabaseRepository implements DeviceRepositoryInterface, DeviceListQuery {
    constructor(
        @InjectRepository(Device) private readonly repository: Repository<Device>
    ) {}

    async save(device: Device): Promise<void> {
        await this.repository.save(device)
    }

    async devices(): Promise<Device[]> {
        return await this.repository.find()
    }

    async findById(id: string): Promise<Device> {
        return await this.repository.findOne({id})
    }
}
