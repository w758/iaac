import * as moment from 'moment'

export class DatetimeProvider {
  private static readonly DEFAULT_FORMAT = 'YYYY-MM-DD HH:mm:ss'

  public static format(format: string): string {
    return moment().format(format)
  }

  public static now(): string {
    return DatetimeProvider.format(DatetimeProvider.DEFAULT_FORMAT)
  }
}
