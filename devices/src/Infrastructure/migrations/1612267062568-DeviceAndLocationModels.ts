import {MigrationInterface, QueryRunner} from 'typeorm';

export class DeviceAndLocationModels1612267062568 implements MigrationInterface {
    name = 'DeviceAndLocationModels1612267062568'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('CREATE TABLE `location` (`id` varchar(255) NOT NULL, `slug` varchar(255) NOT NULL, `name` varchar(255) NOT NULL, `userId` varchar(255) NOT NULL, UNIQUE INDEX `IDX_ff73a7032e673d18bacf8d06c9` (`slug`), UNIQUE INDEX `IDX_f0336eb8ccdf8306e270d400cf` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB');
        await queryRunner.query('CREATE TABLE `device` (`id` varchar(255) NOT NULL, `slug` varchar(255) NOT NULL, `url` varchar(255) NOT NULL, `name` varchar(255) NOT NULL, `type` varchar(255) NOT NULL, `historyLogging` tinyint NOT NULL DEFAULT 0, `locationId` varchar(255) NULL, UNIQUE INDEX `IDX_71e431d65d96046aec01d2d2f2` (`slug`), UNIQUE INDEX `IDX_a9711066605e78033163549682` (`url`), UNIQUE INDEX `IDX_b17080419bd45586a3e78dfcc9` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB');
        await queryRunner.query('ALTER TABLE `device` ADD CONSTRAINT `FK_0a37328dfe6ab8551e94fe585ea` FOREIGN KEY (`locationId`) REFERENCES `location`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE `device` DROP FOREIGN KEY `FK_0a37328dfe6ab8551e94fe585ea`');
        await queryRunner.query('DROP INDEX `IDX_b17080419bd45586a3e78dfcc9` ON `device`');
        await queryRunner.query('DROP INDEX `IDX_a9711066605e78033163549682` ON `device`');
        await queryRunner.query('DROP INDEX `IDX_71e431d65d96046aec01d2d2f2` ON `device`');
        await queryRunner.query('DROP TABLE `device`');
        await queryRunner.query('DROP INDEX `IDX_f0336eb8ccdf8306e270d400cf` ON `location`');
        await queryRunner.query('DROP INDEX `IDX_ff73a7032e673d18bacf8d06c9` ON `location`');
        await queryRunner.query('DROP TABLE `location`');
    }

}
