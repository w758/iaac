import { Module} from '@nestjs/common';
import {ConfigModule} from '@nestjs/config';
import {AppController} from './Infrastructure/controllers/app.controller';
import {CqrsModule} from '@nestjs/cqrs';
import {DeviceController} from './Infrastructure/controllers/device.controller';
import {TypeOrmModule} from '@nestjs/typeorm';
import {Device} from './Domain/device/device.entity';
import {CreateDeviceHandler} from './Application/handlers/create-device.handler';
import {Location} from './Domain/location/location.entity';
import {LocationDatabaseRepository} from './Infrastructure/repositories/location-database.repository';
import {DeviceDatabaseRepository} from './Infrastructure/repositories/device-database.repository';
import {LocationController} from './Infrastructure/controllers/location.controller';
import {CreateLocationHandler} from './Application/handlers/create-location.handler';
import {LoggerModule} from 'nestjs-pino';
import {PromController, PromModule} from "@digikare/nestjs-prom";
import {TerminusModule} from "@nestjs/terminus";
import {HealthController} from "./Infrastructure/controllers/health.controller";

@Module({
  imports: [
    ConfigModule.forRoot(),
    CqrsModule,
    LoggerModule.forRootAsync({
      useFactory: () => {
        return process.env.ENV !== 'prod'
          ? {
            pinoHttp: {
              prettyPrint: {
                colorize: true,
                translateTime: true,
                messageFormat: '[{context}] {msg}',
                ignore: 'context',
              },
              base: null,
              autoLogging: false,
            },
          }
          : {}
      }
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT, 10),
      username: process.env.DATABASE_USERNAME,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      autoLoadEntities: true
    }),
    TypeOrmModule.forFeature([Device, Location]),
    TerminusModule,
    PromModule.forRoot({
      defaultLabels: {
        app: 'Devices',
        version: '1.0.0'
      },
      withHttpMiddleware: {
        enable: true
      },
      withDefaultController: false
    })
  ],
  controllers: [AppController, DeviceController, LocationController, HealthController, PromController.forRoot()],
  providers: [
    DeviceDatabaseRepository,
    LocationDatabaseRepository,
    CreateLocationHandler,
    CreateDeviceHandler,
    {
      provide: 'DeviceRepositoryInterface',
      useExisting: DeviceDatabaseRepository
    },
    {
      provide: 'LocationRepositoryInterface',
      useExisting: LocationDatabaseRepository
    },
    {
      provide: 'DeviceListQuery',
      useExisting: DeviceDatabaseRepository
    },
    {
      provide: 'LocationListQuery',
      useExisting: LocationDatabaseRepository
    },
  ],
})
export class AppModule {}
