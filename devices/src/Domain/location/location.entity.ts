import {Column, Entity, Index, OneToMany, PrimaryColumn} from 'typeorm';
import {Device} from '../device/device.entity';
import {isUUID} from 'class-validator';
import {AssertionError} from 'assert';
import {v4 as uuid} from 'uuid';

@Entity()
export class Location {
    @PrimaryColumn()
    id: string

    @Column()
    @Index({ unique: true })
    slug: string

    @Column()
    @Index({ unique: true })
    name: string

    @Column()
    userId: string

    @OneToMany(type => Device, device => device.location)
    devices: Device[]

    private constructor(
        id: string,
        name: string,
        userId: string,
        slug: string
    ) {
        this.id = id
        this.name = name
        this.slug = slug
        this.userId = userId
    }

    static create(
        name: string,
        userId: string,
        slug: string,
        id?: string
    ): Location {
        if (id && !isUUID(id, 4)) {
            throw new AssertionError({message: `Id "${id}" is not valid uuid v4`})
        }

        if (!isUUID(userId, 4)) {
            throw new AssertionError({message: `User id "${userId}" is not valid uuid v4`})
        }

        return new Location(id ? id : uuid(), name, userId, slug)
    }
}
