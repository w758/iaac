import {Location} from './location.entity';

export interface LocationRepositoryInterface {
    save(location: Location): Promise<void>
    findById(locationId: string): Promise<Location|undefined>
}
