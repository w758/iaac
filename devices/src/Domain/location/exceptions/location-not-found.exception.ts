export class LocationNotFoundException extends Error {
    private constructor(message: string) {
        super(message);
    }

    public static withId(locationId: string) {
        return new LocationNotFoundException(`Location "${locationId}" not found.`)
    }
}
