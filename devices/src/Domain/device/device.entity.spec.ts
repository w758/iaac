import {Type} from './type';
import {AssertionError} from 'assert';
import {Device} from './device.entity';
import {Location} from '../location/location.entity';

describe('Device model', () => {
    const location = Location.create('location', '6ab51f72-1630-4269-828a-b8850591f420', 'slug')

    it('should create device', () => {
        const device = Device.create(
            'http://123.12.12.12:3000',
            'Bathroom Hygrostat',
            Type.HYGROSTAT_AND_THERMOSTAT,
            '28712cb7-0ccb-4f6d-89d4-838ccd966d39',
            location
        )

        const device2 = Device.create(
            'http://123.12.12.12:3000',
            'Bathroom Hygrostat',
            Type.HYGROSTAT_AND_THERMOSTAT,
            '28712cb7-0ccb-4f6d-89d4-838ccd966d39',
            location,
            '2e4aa7b4-239e-463b-a2ca-031f8229c743'
        )

        expect(device.name).toBe('Bathroom Hygrostat')
        expect(device2.id).toBe('2e4aa7b4-239e-463b-a2ca-031f8229c743')
    })

    it('should throw assertion error if url is invalid', () => {
        expect.assertions(2)

        try {
            Device.create(
                'invalidUrl',
                'Bathroom Hygrostat',
                Type.HYGROSTAT_AND_THERMOSTAT,
                'slug',
                location,
            )
        } catch (e) {
            expect(e).toBeInstanceOf(AssertionError);
            expect(e.message).toBe('Url "invalidUrl" is not valid url')
        }
    })

    it('should throw assertion error if id is not valid uuid v4', () => {
        expect.assertions(2)

        try {
            Device.create(
                'http://123.12.12.12:3000',
                'Bathroom Hygrostat',
                Type.HYGROSTAT_AND_THERMOSTAT,
                'slug',
                location,
                'notUuid'
            )
        } catch (e) {
            expect(e).toBeInstanceOf(AssertionError);
            expect(e.message).toBe('Id "notUuid" is not valid uuid v4')
        }
    })

    it('should throw assertion error if type is not defined', () => {
        expect.assertions(2)

        try {
            Device.create(
                'http://123.12.12.12:3000',
                'Bathroom Hygrostat',
                // @ts-ignore
                'type_not_defined',
                '28712cb7-0ccb-4f6d-89d4-838ccd966d39',
                location
            )
        } catch (e) {
            expect(e).toBeInstanceOf(AssertionError);
            expect(e.message).toBe('Type should be from defined types')
        }
    })
})
