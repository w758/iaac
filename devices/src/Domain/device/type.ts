export enum Type {
    SMART_PLUG = 'smart-plug',
    HYGROSTAT_AND_THERMOSTAT = 'hygrostat-and-thermostat',
    REMOTE_GATE = 'remote-gate',
    ENERGY_METER = 'energy-meter',
    GAS_SENSOR = 'gas-sensor',
    KEYPAD = 'keypad',
    RGB_LED_DRIVER = 'rgb-led-driver',
    METEO_STATION = 'meteo-station',
    GROWBOX = 'growbox'
}
