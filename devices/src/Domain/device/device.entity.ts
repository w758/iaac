import {Column, Entity, Index, ManyToOne, PrimaryColumn} from 'typeorm';
import {Type} from './type';
import {v4 as uuid} from 'uuid'
import {isEnum, isURL, isUUID} from 'class-validator';
import {AssertionError} from 'assert';
import {Location} from '../location/location.entity';
import {Exclude} from 'class-transformer';

@Entity()
export class Device {
    @PrimaryColumn()
    id: string

    @Column()
    @Index({ unique: true })
    slug: string

    @Column()
    @Index({ unique: true })
    url: string

    @Column()
    @Index({ unique: true })
    name: string

    @Column()
    type: Type

    @Column({default: false})
    historyLogging: boolean

    @Exclude()
    @ManyToOne(type => Location)
    location: Location

    @Column()
    locationId: string

    private constructor(
        id: string,
        url: string,
        name: string,
        type: Type,
        slug: string,
        location: Location
    ) {
        this.id = id
        this.name = name
        this.url = url
        this.type = type
        this.slug = slug
        this.location = location
    }

    static create(
        url: string,
        name: string,
        type: Type,
        slug: string,
        location: Location,
        id?: string
    ): Device {
        if (id && !isUUID(id, 4)) {
            throw new AssertionError({message: `Id "${id}" is not valid uuid v4`})
        }

        if (!isURL(url)) {
            throw new AssertionError({message: `Url "${url}" is not valid url`})
        }

        if (!isEnum(type, Type)) {
            throw new AssertionError({message: `Type should be from defined types`})
        }

        return new Device(id ? id : uuid(), url, name, type, slug, location)
    }
}
