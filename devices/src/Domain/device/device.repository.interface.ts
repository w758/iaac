import {Device} from './device.entity';

export interface DeviceRepositoryInterface {
    save(device: Device): Promise<void>
    findById(id: string): Promise<Device>
}
