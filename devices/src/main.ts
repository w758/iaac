import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import axios from 'axios';
import {ValidationPipe} from '@nestjs/common';
import {Logger, PinoLogger} from 'nestjs-pino';
import {NestExpressApplication} from '@nestjs/platform-express';

async function bootstrap() {
  const loggerConfig =
    process.env.ENV !== 'prod'
      ? {
        pinoHttp: {
          prettyPrint: {
            colorize: true,
            translateTime: true,
            messageFormat: '[{context}] {msg}',
            ignore: 'context',
          },
          base: null,
          autoLogging: false,
        },
      }
      : {};

  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    logger: new Logger(new PinoLogger(loggerConfig), {}),
    cors: {
      origin: '*',
    },
  });

  // enableAxiosLogger(logger);
  // TODO: LoggerMiddleware doesnt work (error: logger is undefined and server return 500)
  // app.use(LoggerMiddleware)

  app.useGlobalPipes(new ValidationPipe({
    transform: true,
  }))

  await app.listen(process.env.HTTP_PORT);
}

async function enableAxiosLogger(logger) {
  axios.interceptors.request.use((request) => {
    logger.debug(
        `AxiosRequest: ${request.url} with body: ${JSON.stringify(request.data)}`,
        request.method.toUpperCase(),
    );
    return request;
  });

  axios.interceptors.response.use((response) => {
    logger.debug(
        `AxiosResponse: ${response.config.url} with body: ${JSON.stringify(
            response.data,
        )}`,
        response.request.method.toUpperCase(),
    );
    return response;
  });
}

bootstrap();
