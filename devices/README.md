 ## First start with Docker

```bash
$ cp .env.dist .env
$ docker build -t report-generator .
$ docker-compose up -d
```
## First start without Docker

```bash
$ cp .env.dist .env
$ npm i
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
## Commands
Command to download report template. `latest` is version of template and means latest version from bucket. In other way we can pass any version name like `template1`
```
npx nestjs-command download:report-template latest
#or
npx nestjs-command download:report-template template1
```

## Change log level
In `.env` file set `LOG_LEVEL` to one of the possible values:
* error,
* warn,
* info,
* http,
* verbose,
* debug,
* silly

More information: https://github.com/winstonjs/winston

## Generate keys for RabbitMQ
Inside folder with keys (for NodeJS inside `./keys`)
```
$ ssh-keygen -t rsa -b 4096 -m PEM -f self.key
$ openssl rsa -in self.key -pubout -outform PEM -out self.key.pub
```
**Then pass public key to application that consumes messages from ReportGenerator**

## TypeORM

### Generate migration
```
npm run typeorm migration:generate -- -n DeviceModel
```
### Run migrations

```
npm run typeorm migration:run
```
