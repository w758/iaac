import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import axios from 'axios';
import {WinstonAdapterLogger} from './Infrastructure/logger/winston-adapter.logger';
import {ValidationPipe} from '@nestjs/common';

async function bootstrap() {
  const logger = new WinstonAdapterLogger();

  enableAxiosLogger(logger);

  const app = await NestFactory.create(AppModule, {
    cors: {
      origin: '*',
    },
    logger,
  });

  app.useGlobalPipes(new ValidationPipe({
    transform: true,
  }))

  await app.listen(process.env.HTTP_PORT);
}

async function enableAxiosLogger(logger) {
  axios.interceptors.request.use((request) => {
    logger.debug(
      `AxiosRequest: ${request.url} with body: ${JSON.stringify(request.data)}`,
      request.method.toUpperCase(),
    );
    return request;
  });

  axios.interceptors.response.use((response) => {
    logger.debug(
      `AxiosResponse: ${response.config.url} with body: ${JSON.stringify(
        response.data,
      )}`,
      response.request.method.toUpperCase(),
    );
    return response;
  });
}

bootstrap();
