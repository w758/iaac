import {
  Controller,
  Get,
  HttpStatus,
  Inject,
  LoggerService,
  Res,
} from '@nestjs/common'
import { Response } from 'express'
import {DevicePropertyQuery} from '../../Application/queries/device-property.query';
import {QueryBus} from '@nestjs/cqrs';

@Controller('/weather')
export class WeatherController {
  constructor(
    @Inject('LoggerService') private readonly logger: LoggerService,
    private readonly queryBus: QueryBus
  ) {}

  @Get('/temperature')
  async get(@Res() res: Response): Promise<any> {
    try {
      const query = new DevicePropertyQuery('83228c90-24b7-4a7e-8323-b91651bd374d', 'temperature-outside')

      return res
          .status(HttpStatus.OK)
          .json(await this.queryBus.execute(query))
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: {
          message: e.message,
        },
      });
    }
  }
}
