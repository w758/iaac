import {
    Controller,
    Get,
    Post,
    HttpStatus,
    Inject,
    LoggerService, Param, Query,
    Res, Body, Patch, Put,
} from '@nestjs/common'
import { Response } from 'express'
import {CommandBus, QueryBus} from '@nestjs/cqrs';
import {DevicePropertyQuery} from '../../Application/queries/device-property.query';
import {DevicesFilterOptionsDto} from '../../Application/dtos/devices-filter-options.dto';
import {DeviceListQuery} from '../../Application/queries/device-list.query';
import {DeviceDto} from '../../Application/dtos/device.dto';
import {DevicesHttpClient} from '../http-clients/devices/devices.http-client';
import {PropertySearchCriteria} from '../../Application/search-criteria/property.search-criteria';
import {DevicePropertyHistoryQuery} from '../../Application/queries/device-property-history.query';
import {DeviceConfigQuery} from '../../Application/queries/device-config.query';
import {UpdateDeviceConfigCommand} from '../../Application/commands/update-device-config.command';
import {RunDeviceActionCommand} from '../../Application/commands/run-device-action.command';
import {DeviceAllPropertiesQuery} from '../../Application/queries/device-all-properties.query';

@Controller('/devices')
export class DeviceController {
    constructor(
        @Inject('LoggerService') private readonly logger: LoggerService,
        private readonly commandBus: CommandBus,
        private readonly queryBus: QueryBus,
        private readonly devicesHttpClient: DevicesHttpClient,
        @Inject('DeviceListQuery') private readonly devicesQuery: DeviceListQuery,
    ) {}

    @Get()
    async list(@Res() res: Response, @Query() query: DevicesFilterOptionsDto): Promise<any> {
        try {
            return res
                .status(HttpStatus.OK)
                .json(await this.devicesQuery.devices(query));
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                error: {
                    message: e.message,
                },
            });
        }
    }

    @Post()
    async create(
        @Res() res: Response,
        @Body() deviceDto: DeviceDto
    ): Promise<any> {
        try {
            const device = await this.devicesHttpClient.createDevice(deviceDto)

            return res.status(201).send(device)
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                error: {
                    message: e.message,
                },
            });
        }
    }

    @Get('/:id/:property')
    async getProperty(
        @Res() res: Response,
        @Param('id') deviceId: string,
        @Param('property') property: string
    ): Promise<any> {
        try {
            const query = new DevicePropertyQuery(deviceId, property)

            return res
                .status(HttpStatus.OK)
                .json(await this.queryBus.execute(query))
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                error: {
                    message: e.message,
                },
            });
        }
    }

    @Get('/:id')
    async getProperties(
        @Res() res: Response,
        @Param('id') deviceId: string
    ): Promise<any> {
        try {
            const query = new DeviceAllPropertiesQuery(deviceId)

            return res
                .status(HttpStatus.OK)
                .json(await this.queryBus.execute(query))
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                error: {
                    message: e.message,
                },
            });
        }
    }

    @Get('/:id/config')
    async config(
        @Res() res: Response,
        @Param('id') deviceId: string
    ): Promise<any> {
        try {
            const query = new DeviceConfigQuery(deviceId)

            return res
                .status(HttpStatus.OK)
                .json(await this.queryBus.execute(query))
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                error: {
                    message: e.message,
                },
            });
        }
    }

    @Put('/:id/config')
    async updateConfig(
        @Res() res: Response,
        @Param('id') deviceId: string,
        @Body() config: Record<string, any>
    ): Promise<any> {
        try {
            const command = new UpdateDeviceConfigCommand(deviceId, config)

            return res
                .status(HttpStatus.OK)
                .json(await this.commandBus.execute(command))
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                error: {
                    message: e.message,
                },
            });
        }
    }

    @Post('/:id/actions/:action')
    async runAction(
        @Res() res: Response,
        @Param('id') deviceId: string,
        @Param('action') action: string,
        @Body() payload: Record<string, any>
    ): Promise<any> {
        try {
            const command = new RunDeviceActionCommand(deviceId, action, payload)

            return res
                .status(HttpStatus.OK)
                .json(await this.commandBus.execute(command))
        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                error: {
                    message: e.message,
                },
            });
        }
    }
}
