import {Controller, Get, HttpStatus, Res, Body, Post, Inject} from '@nestjs/common';
import {Response} from 'express'
import {CommandBus} from '@nestjs/cqrs';
import {LocationDto} from '../../Application/dtos/location.dto';
import {LocationListQuery} from '../../Application/queries/location-list.query';
import {DevicesHttpClient} from '../http-clients/devices/devices.http-client';
import {LocationsFilterOptionsDto} from '../../Application/dtos/locations-filter-options.dto';

@Controller('/locations')
export class LocationController {
  constructor(
      private readonly commandBus: CommandBus,
      private readonly devicesHttpClient: DevicesHttpClient,
      @Inject('LocationListQuery') private readonly locationList: LocationListQuery
  ) {}

  @Get()
  async listAction(
      @Res() res: Response
  ): Promise<any> {
    try {
      const filterOptions = new LocationsFilterOptionsDto();

      filterOptions.userId = 'c1fdfde1-aacf-442d-b3b6-2add38537812'

      return res.status(200).send(await this.locationList.locations(filterOptions))
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: {
          message: e.message,
        },
      });
    }
  }

  @Post()
  async createAction(
      @Res() res: Response,
      @Body() locationDto: LocationDto
  ): Promise<any> {
    try {
      const location = await this.devicesHttpClient.createLocation(locationDto)

      return res.status(201).send(location)
    } catch (e) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        error: {
          message: e.message,
        },
      });
    }
  }
}
