import {
  Controller,
  Get,
  HttpStatus,
  Inject,
  LoggerService,
  Res,
} from '@nestjs/common'
import { Response } from 'express'

@Controller('/')
export class AppController {
  constructor(
    @Inject('LoggerService') private readonly logger: LoggerService,
  ) {}

  @Get('')
  async get(@Res() res: Response): Promise<any> {
    const data = {
      name: 'Backend',
      description: 'Main backend of IoT infrastructure in my home'
    }

    return res
      .status(HttpStatus.OK)
      .json(data)
  }
}
