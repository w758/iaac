import { Injectable } from '@nestjs/common';
import axios from 'axios';
import {Device} from '../../../Domain/device/device';
import {Property, SmartDeviceInterface} from '../../../Application/interfaces/smart-device.interface';

@Injectable()
export class SmartDeviceHttpClient implements SmartDeviceInterface {
    async propertyValue(device: Device, property: string): Promise<Property> {
        try {
            const response = await axios.get(`${device.url}/api/v1/${property}`)

            return response.data
        } catch (e) {
            const message = `Cannot get device "${device.url}" property "${property}" value, becouse: ${e.message}`;

            // TODO: if 404 returned exception UnsupportedPropertyException

            throw new Error(message);
        }
    }

    async properties(device: Device): Promise<Property[]> {
        try {
            const response = await axios.get(`${device.url}/api/v1/properties`)

            return response.data
        } catch (e) {
            const message = `Cannot get device "${device.url}" properties, becouse: ${e.message}`;

            throw new Error(message);
        }
    }

    async config(device: Device): Promise<Device> {
        try {
            const response = await axios.get(`${device.url}/api/v1/config`)

            return {
                ...device,
                config: response.data
            }
        } catch (e) {
            const message = `Cannot get device "${device.url}" config, becouse: ${e.message}`;

            throw new Error(message);
        }
    }

    async updateConfig(device: Device): Promise<Record<string, any>> {
        try {
            const response = await axios.put(`${device.url}/api/v1/config`, device.config)

            return response.data
        } catch (e) {
            const message = `Cannot update device "${device.url}" config, becouse: ${e.message}`;

            throw new Error(message);
        }
    }

    async runAction(device: Device, action: string, payload: Record<string, any>): Promise<Record<string, any>> {
        try {
            const response = await axios.post(`${device.url}/api/v1/actions/${action}`, payload)

            return response.data
        } catch (e) {
            const message = `Cannot run action on device "${device.url}", becouse: ${e.message}`;

            throw new Error(message);
        }
    }
}
