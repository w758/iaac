import { Inject, Injectable, LoggerService } from '@nestjs/common';
import axios from 'axios';
import {DeviceListQuery} from '../../../Application/queries/device-list.query';
import {LocationListQuery} from '../../../Application/queries/location-list.query';
import {DeviceDto} from '../../../Application/dtos/device.dto';
import {DevicesFilterOptionsDto} from '../../../Application/dtos/devices-filter-options.dto';
import {LocationDto} from '../../../Application/dtos/location.dto';
import {LocationsFilterOptionsDto} from '../../../Application/dtos/locations-filter-options.dto';
import {Device} from '../../../Domain/device/device';
import {DeviceRepositoryInterface} from '../../../Domain/device/device.repository.interface';

@Injectable()
export class DevicesHttpClient implements DeviceListQuery, LocationListQuery, DeviceRepositoryInterface {
  constructor(
    @Inject('LoggerService') private readonly logger: LoggerService,
  ) {}

  public async devices(filterOptions: DevicesFilterOptionsDto): Promise<any> {
    try {
      const response = await axios.get(`${process.env.DEVICES_URL}/devices`, {
        params: filterOptions
      })

      return response.data
    } catch (error) {
      const message = `Cannot fetch devices, because: ${error.response.data.error.message}`;

      this.logger.error(error);

      throw new Error(message);
    }
  }

  public async findById(id: string): Promise<Device> {
    try {
      const response = await axios.get(`${process.env.DEVICES_URL}/devices/${id}`)

      return response.data
    } catch (error) {
      const message = `Cannot fetch device ${id}, because: ${error.response.data.error.message}`;

      this.logger.error(error);

      throw new Error(message);
    }
  }

  public async locations(filterOptions?: LocationsFilterOptionsDto): Promise<any> {
    try {
      const response = await axios.get(`${process.env.DEVICES_URL}/locations`, {
        params: filterOptions
      })

      return response.data
    } catch (error) {
      const message = `Cannot fetch locations, because: ${error.response.data.error.message}`;

      this.logger.error(error);

      throw new Error(message);
    }
  }


  public async createDevice(device: DeviceDto): Promise<any> {
    try {
      const response = await axios.post(
          `${process.env.DEVICES_URL}/devices`, device
      )

      return response.data
    } catch (error) {
      const message = `Cannot create device, because: ${error.response.data.error.message}`;

      this.logger.error(error);

      throw new Error(message);
    }
  }

  public async createLocation(location: LocationDto): Promise<any> {
    try {
      const response = await axios.post(
          `${process.env.DEVICES_URL}/locations`, location
      )

      return response.data
    } catch (error) {
      const message = `Cannot create location, because: ${error.response.data.error.message}`;

      this.logger.error(error);

      throw new Error(message);
    }
  }
}
