import {LoggerService} from '@nestjs/common';
import * as Winston from 'winston';
import {Logger} from 'winston';
import * as Moment from 'moment'

export class WinstonAdapterLogger implements LoggerService {
    private static winstonInstance: Logger = null

    constructor() {
        if (WinstonAdapterLogger.winstonInstance === null) {
            const {format} = Winston
            const logFormat = format.printf(({ level, message, label, timestamp }) => {
                return `${WinstonAdapterLogger.timestampFormat()} [${label}] ${level}: ${message}`
            })

            WinstonAdapterLogger.winstonInstance = Winston.createLogger({
                level: process.env.LOG_LEVEL,
                format: Winston.format.combine(
                    Winston.format.colorize(),
                    logFormat,
                ),
                transports: [
                    new Winston.transports.File({filename: `${__dirname}/../../../logs/${process.env.ENV}.log`}),
                ],
            })

            if (process.env.ENV !== 'prod') {
                WinstonAdapterLogger.winstonInstance.add(new Winston.transports.Console({
                    format: Winston.format.combine(
                        logFormat,
                    ),
                }));
            }
        }
    }

    private static timestampFormat() {
        return Moment().format('YYYY-MM-DD hh:mm:ss').trim()
    }

    public debug(message: string, context?: string): any {
        WinstonAdapterLogger.winstonInstance.debug({level: 'debug', message, label: context})
    }

    public error(message: string, trace?: string, context?: string): any {
        WinstonAdapterLogger.winstonInstance.error({level: 'error', message, label: context})
    }

    public log(message: string, context?: string): any {
        WinstonAdapterLogger.winstonInstance.log({level: 'info', message, label: context})
    }

    public verbose(message: string, context?: string): any {
        WinstonAdapterLogger.winstonInstance.verbose({level: 'verbose', message, label: context})
    }

    public warn(message: string, context?: string): any {
        WinstonAdapterLogger.winstonInstance.warn({level: 'warn', message, label: context})
    }
}
