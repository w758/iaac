import * as moment from 'moment'
import { DatetimeProvider } from './datetime.provider'

describe('Datetime provider', () => {
  // @ts-ignore
  moment.now = () => {
    return '2019-12-26T15:51:26+00:00'
  }

  // @ts-ignore
  moment.utc = () => {
    return '+00:00'
  }

  it('should return current datetime with default format and utc 0', () => {
    expect(DatetimeProvider.now()).toBe('2019-12-26 15:51:26')
  })

  it('should return current datetime with proper format', () => {
    expect(DatetimeProvider.format('YYYY-MM-DD')).toBe('2019-12-26')
  })
})
