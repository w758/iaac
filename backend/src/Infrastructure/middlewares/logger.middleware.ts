import {
  Inject,
  Injectable,
  LoggerService,
  NestMiddleware,
} from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  constructor(
    @Inject('LoggerService') private readonly logger: LoggerService,
  ) {}

  use(req: Request, res: Response, next: () => void): void {
    this.logger.debug(
      `Route: ${req.protocol}://${req.get('host')}${req.originalUrl} with body: ${JSON.stringify(
        req.body,
      )} and headers: ${JSON.stringify(req.headers)}`,
      req.method,
    );
    next();
  }
}
