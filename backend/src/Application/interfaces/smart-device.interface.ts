import {Device} from '../../Domain/device/device';

export type Property = {
    value: any,
    unit?: string
}

export interface SmartDeviceInterface {
    propertyValue(device: Device, property: string): Promise<Property>
    properties(device: Device): Promise<Property[]>
    config(device: Device): Promise<Device>
    updateConfig(device: Device): Promise<Record<string, any>>
    runAction(device: Device, action: string, payload: Record<string, any>): Promise<Record<string, any>>
}
