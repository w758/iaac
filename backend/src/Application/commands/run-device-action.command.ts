export class RunDeviceActionCommand {
  constructor(
    public readonly deviceId: string,
    public readonly action: string,
    public readonly payload: Record<string, any>
  ) {}
}
