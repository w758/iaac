export class UpdateDeviceConfigCommand {
  constructor(
    public readonly deviceId: string,
    public readonly config: Record<string, any>
  ) {}
}
