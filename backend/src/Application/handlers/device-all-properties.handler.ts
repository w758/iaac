import {IQueryHandler, QueryHandler} from '@nestjs/cqrs';
import {Inject} from '@nestjs/common';
import {DevicePropertyQuery} from '../queries/device-property.query';
import {DeviceRepositoryInterface} from '../../Domain/device/device.repository.interface';
import {Property, SmartDeviceInterface} from '../interfaces/smart-device.interface';
import {DeviceAllPropertiesQuery} from '../queries/device-all-properties.query';

@QueryHandler(DeviceAllPropertiesQuery)
export class DeviceAllPropertiesHandler implements IQueryHandler<DeviceAllPropertiesQuery> {
    constructor(
        @Inject('SmartDeviceInterface') private readonly smartDevice: SmartDeviceInterface,
        @Inject('DeviceRepositoryInterface') private readonly deviceRepository: DeviceRepositoryInterface
    ) {}

    async execute(query: DeviceAllPropertiesQuery): Promise<Property[]> {
        const device = await this.deviceRepository.findById(query.deviceId)

        return this.smartDevice.properties(device)
    }
}
