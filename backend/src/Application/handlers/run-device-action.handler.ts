import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Inject } from '@nestjs/common';
import {UpdateDeviceConfigCommand} from '../commands/update-device-config.command';
import {SmartDeviceInterface} from '../interfaces/smart-device.interface';
import {DeviceRepositoryInterface} from '../../Domain/device/device.repository.interface';
import {Device} from '../../Domain/device/device';
import {RunDeviceActionCommand} from '../commands/run-device-action.command';

@CommandHandler(RunDeviceActionCommand)
export class RunDeviceActionHandler
  implements ICommandHandler<RunDeviceActionCommand> {
  constructor(
      @Inject('SmartDeviceInterface') private readonly smartDevice: SmartDeviceInterface,
      @Inject('DeviceRepositoryInterface') private readonly deviceRepository: DeviceRepositoryInterface
  ) {}

  async execute(command: RunDeviceActionCommand): Promise<Record<string, any>> {
    const device = await this.deviceRepository.findById(command.deviceId)

    return this.smartDevice.runAction(device, command.action, command.payload);
  }
}
