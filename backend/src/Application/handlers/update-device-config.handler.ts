import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Inject } from '@nestjs/common';
import {UpdateDeviceConfigCommand} from '../commands/update-device-config.command';
import {SmartDeviceInterface} from '../interfaces/smart-device.interface';
import {DeviceRepositoryInterface} from '../../Domain/device/device.repository.interface';
import {Device} from '../../Domain/device/device';

@CommandHandler(UpdateDeviceConfigCommand)
export class UpdateDeviceConfigHandler
  implements ICommandHandler<UpdateDeviceConfigCommand> {
  constructor(
      @Inject('SmartDeviceInterface') private readonly smartDevice: SmartDeviceInterface,
      @Inject('DeviceRepositoryInterface') private readonly deviceRepository: DeviceRepositoryInterface
  ) {}

  async execute(command: UpdateDeviceConfigCommand): Promise<Record<string, any>> {
    const device = await this.deviceRepository.findById(command.deviceId)

    return this.smartDevice.updateConfig({
      ...device,
      config: command.config
    });
  }
}
