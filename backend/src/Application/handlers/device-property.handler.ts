import {IQueryHandler, QueryHandler} from '@nestjs/cqrs';
import {Inject} from '@nestjs/common';
import {DevicePropertyQuery} from '../queries/device-property.query';
import {DeviceRepositoryInterface} from '../../Domain/device/device.repository.interface';
import {Property, SmartDeviceInterface} from '../interfaces/smart-device.interface';

@QueryHandler(DevicePropertyQuery)
export class DevicePropertyHandler implements IQueryHandler<DevicePropertyQuery> {
    constructor(
        @Inject('SmartDeviceInterface') private readonly smartDevice: SmartDeviceInterface,
        @Inject('DeviceRepositoryInterface') private readonly deviceRepository: DeviceRepositoryInterface
    ) {}

    async execute(query: DevicePropertyQuery): Promise<Property> {
        const device = await this.deviceRepository.findById(query.deviceId)

        return this.smartDevice.propertyValue(device, query.property)
    }
}
