import {IQueryHandler, QueryHandler} from '@nestjs/cqrs';
import {Inject} from '@nestjs/common';
import {DeviceRepositoryInterface} from '../../Domain/device/device.repository.interface';
import {SmartDeviceInterface} from '../interfaces/smart-device.interface';
import {DeviceConfigQuery} from '../queries/device-config.query';

@QueryHandler(DeviceConfigQuery)
export class DeviceConfigHandler implements IQueryHandler<DeviceConfigQuery> {
    constructor(
        @Inject('SmartDeviceInterface') private readonly smartDevice: SmartDeviceInterface,
        @Inject('DeviceRepositoryInterface') private readonly deviceRepository: DeviceRepositoryInterface
    ) {}

    async execute(query: DeviceConfigQuery): Promise<any> {
        const device = await this.deviceRepository.findById(query.deviceId)

        return this.smartDevice.config(device)
    }
}
