export class DevicePropertyQuery {
  constructor(
      public readonly deviceId: string,
      public readonly property: string,
  ) {}
}
