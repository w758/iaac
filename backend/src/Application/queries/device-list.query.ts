import {DevicesFilterOptionsDto} from '../dtos/devices-filter-options.dto';

export interface DeviceListQuery {
  devices(filterOptions: DevicesFilterOptionsDto): Promise<any>;
}
