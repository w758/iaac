import {LocationsFilterOptionsDto} from '../dtos/locations-filter-options.dto';

export interface LocationListQuery {
    locations(filterOptions?: LocationsFilterOptionsDto): Promise<any>
}
