import {PropertySearchCriteria} from '../search-criteria/property.search-criteria';

export interface DevicePropertyHistoryQuery {
  propertyHistory(
      deviceId: string,
      property: string,
      searchCriteria: PropertySearchCriteria
  ): Promise<any>
}
