export interface PropertySearchCriteria {
    datetimeFrom: string
    datetimeTo: string
    resolution: string
}
