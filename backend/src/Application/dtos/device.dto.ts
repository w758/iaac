import {IsEnum, IsString, IsUrl, IsUUID} from 'class-validator';

export class DeviceDto {
    @IsString()
    public readonly name: string

    @IsUrl()
    public readonly url: string

    @IsString()
    public readonly type: string

    @IsUUID(4)
    public readonly locationId: string
}
