import {IsOptional, IsString} from 'class-validator';

export class DevicesFilterOptionsDto {
  @IsString()
  @IsOptional()
  public readonly type: string;
}
