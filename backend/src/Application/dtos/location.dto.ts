import { IsString, IsUUID } from 'class-validator';

export class LocationDto {
    @IsString()
    public readonly name: string

    @IsUUID(4)
    public readonly userId: string
}
