import {IsOptional, IsUUID} from 'class-validator';

export class LocationsFilterOptionsDto {
    @IsUUID(4)
    @IsOptional()
    userId: string
}
