import { Mode } from './Mode'

export interface Config {
  humidityThreshold: number;
  temperatureThreshold: number;
  mode: Mode;
}
