export enum Mode {
  MANUAL = 'manual',
  THERMOSTAT = 'thermostat',
  HYGROSTAT = 'hygrostat',
}
