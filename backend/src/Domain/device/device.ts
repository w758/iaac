export type Config = Record<string, any>

export type Device = {
    id: string,
    url: string,
    config?: Config
}
