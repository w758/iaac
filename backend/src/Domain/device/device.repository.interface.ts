import {Device} from './device';

export interface DeviceRepositoryInterface {
    findById(id: string): Promise<Device>
}
