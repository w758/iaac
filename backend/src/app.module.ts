import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { CqrsModule } from '@nestjs/cqrs';
import { AppController } from './Infrastructure/controllers/app.controller';
import { LoggerMiddleware } from './Infrastructure/middlewares/logger.middleware';
import {WinstonAdapterLogger} from './Infrastructure/logger/winston-adapter.logger';
import {WeatherController} from './Infrastructure/controllers/weather.controller';
import {DeviceController} from './Infrastructure/controllers/device.controller';
import {LocationController} from './Infrastructure/controllers/location.controller';
import {DevicesHttpClient} from './Infrastructure/http-clients/devices/devices.http-client';
import {DevicePropertyHandler} from './Application/handlers/device-property.handler';
import {SmartDeviceHttpClient} from './Infrastructure/http-clients/smart-device/smart-device.http-client';
import {DeviceConfigHandler} from './Application/handlers/device-config.handler';
import {UpdateDeviceConfigHandler} from './Application/handlers/update-device-config.handler';
import {RunDeviceActionHandler} from './Application/handlers/run-device-action.handler';
import {DeviceAllPropertiesHandler} from './Application/handlers/device-all-properties.handler';
import {PromController, PromModule} from "@digikare/nestjs-prom";
import {TerminusModule} from "@nestjs/terminus";
import {HealthController} from "./Infrastructure/controllers/health.controller";

const commandHandlers = [
  UpdateDeviceConfigHandler,
  RunDeviceActionHandler
];

const queryHandlers = [
  DevicePropertyHandler,
  DeviceConfigHandler,
  DeviceAllPropertiesHandler
]

@Module({
  imports: [ConfigModule.forRoot(), TerminusModule, CqrsModule, PromModule.forRoot({
    defaultLabels: {
      app: 'Backend',
      version: '1.0.0'
    },
    withHttpMiddleware: {
      enable: true
    },
    withDefaultController: false
  })],
  controllers: [
    AppController,
    WeatherController,
    DeviceController,
    LocationController,
    PromController.forRoot(),
    HealthController
  ],
  providers: [
    ...commandHandlers,
    ...queryHandlers,
    DevicesHttpClient,
    SmartDeviceHttpClient,
    {
      provide: 'LoggerService',
      useClass: WinstonAdapterLogger,
    },
    {
      provide: 'DeviceListQuery',
      useExisting: DevicesHttpClient,
    },
    {
      provide: 'LocationListQuery',
      useExisting: DevicesHttpClient,
    },
    {
      provide: 'DeviceRepositoryInterface',
      useExisting: DevicesHttpClient,
    },
    {
      provide: 'SmartDeviceInterface',
      useExisting: SmartDeviceHttpClient
    }
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(LoggerMiddleware).forRoutes('/');
  }
}
