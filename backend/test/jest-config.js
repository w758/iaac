require('dotenv').config({path: '.env.test'})

if (process.env.ENV !== 'test') {
  throw Error("Loaded envs do not belong to the test environment")
}
