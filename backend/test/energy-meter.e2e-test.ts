import * as request from 'supertest'
import { Test } from '@nestjs/testing'
import { AppModule } from '../src/app.module'
import {INestApplication, ValidationPipe} from '@nestjs/common'
import * as http from 'http';
import * as mockserver from 'mockserver'
import {Server} from 'http';

describe('EnergyMeterController (e2e)', () => {
  let app: INestApplication
  let mockServer: Server

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    app.useGlobalPipes(new ValidationPipe({
      transform: true,
    }))

    await app.init()

    mockServer = http.createServer(mockserver('test/mocks/energy-meter')).listen(9001)
  })

  afterAll(() => {
    mockServer.close()
  })

  it('/api/v1/energy (GET)', () => {
    const result = {
      value: '10',
      unit: 'Wh'
    }

    return request(app.getHttpServer())
      .get('/energy-meter')
      .expect(200)
      .expect(JSON.stringify(result))
  })

  it('/api/v1/energy (POST) - bad initial value', () => {
    const result = {
      error: {
        message: 'Cannot set initial energy value. Request failed with status code 400'
      },
    }
    const body = {
      initialValue: -1,
    }

    return request(app.getHttpServer())
      .post('/energy-meter')
      .set('Accept', 'application/json')
      .send(body)
      .expect(400)
      .expect(JSON.stringify(result))
  })

  it('/api/v1/energy (POST) - without pass initial value', () => {
    const result = {
      statusCode: 400,
      message: ['initialValue must be a number conforming to the specified constraints'],
      error: 'Bad Request'
    }

    return request(app.getHttpServer())
      .post('/energy-meter')
      .set('Accept', 'application/json')
      .send({})
      .expect(400)
      .expect(JSON.stringify(result))
  })

  it('/api/v1/energy (POST) - success', () => {
    const result = {
      value: '10',
      unit: 'Wh'
    }
    const body = {
      initialValue: 11,
    }

    return request(app.getHttpServer())
      .post('/energy-meter')
      .set('Accept', 'application/json')
      .send(body)
      .expect(200)
      .expect(JSON.stringify(result))
  })
})
