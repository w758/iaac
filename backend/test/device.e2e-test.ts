import * as request from 'supertest'
import { Test } from '@nestjs/testing'
import { AppModule } from '../src/app.module'
import {INestApplication, ValidationPipe} from '@nestjs/common'
import * as http from 'http';
import * as mockserver from 'mockserver'
import {Server} from 'http';
import {setupServer} from 'msw/node';
import {rest} from 'msw';

describe('HygrostatAndThermostatController (e2e)', () => {
  let app: INestApplication
  let mockServer: Server

  const httpMockServer = setupServer(
      rest.post('http://webhook-url', (_, res) => res()),
      rest.post('http://timeout-webhook-url', (_, res, ctx) => {
        return res();
      }),
  );

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    app.useGlobalPipes(new ValidationPipe({
      transform: true,
    }))

    await app.init()

    httpMockServer.listen({onUnhandledRequest: 'bypass'})
    mockServer = http.createServer(mockserver('test/mocks/hygrostat-and-thermostat')).listen(9002)
  })

  afterAll(() => {
    mockServer.close()
  })

  it('/api/v1/temperature (GET)', () => {
    const result = {
      value: '22.20',
    }

    return request(app.getHttpServer())
      .get('/hygrostat-and-thermostat/temperature')
      .expect(200)
      .expect(JSON.stringify(result))
  })

  it('/api/v1/humidity (GET)', () => {
    const result = {
      value: '56.20',
    }

    return request(app.getHttpServer())
      .get('/hygrostat-and-thermostat/humidity')
      .expect(200)
      .expect(JSON.stringify(result))
  })
})
