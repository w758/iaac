provider "azurerm" {
  features {}
}

variable "agent_count" {
    default = 1
}

variable "ssh_public_key" {
    default = "~/.ssh/id_rsa.pub"
}

variable "dns_prefix" {
    default = "k8sDashboard"
}

variable cluster_name {
    default = "k8sDashboard"
}

variable resource_group_name {
    default = "k8s-dashboard-group"
}

variable location {
    default = "eastus"
}

resource "azurerm_resource_group" "k8s" {
    name     = var.resource_group_name
    location = var.location
}

resource "azurerm_kubernetes_cluster" "k8s" {
    name                = var.cluster_name
    location            = azurerm_resource_group.k8s.location
    resource_group_name = azurerm_resource_group.k8s.name
    dns_prefix          = var.dns_prefix

    default_node_pool {
        name            = "agentpool"
        node_count      = var.agent_count
        vm_size         = "Standard_D2_v2"
    }

    identity {
        type = "SystemAssigned"
    }

    network_profile {
        load_balancer_sku = "Standard"
        network_plugin = "kubenet"
    }
}