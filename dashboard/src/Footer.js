import React from "react";
import moment from "moment";

export const Footer = () => {
  return (
    <footer className="sticky-footer bg-white">
      <div className="container my-auto">
        <div className="copyright text-center my-auto">
          <span>Copyright &copy; Kuba Filinger {moment().format("YYYY")}</span>
        </div>
      </div>
    </footer>
  );
};
