import "./scss/main.scss";
import React from "react";
import ReactDOM from "react-dom";
import { Dashboard } from "./Dashboard";
import { BrowserRouter as Router } from "react-router-dom";
import { AlertProvider } from "./contexts/AlertContext";
import { UserProvider } from "./contexts/UserContext";
import { DevicesProvider } from "./contexts/DevicesContext";
import { LocationsProvider } from "./contexts/LocationsContext";
import './axios-interceptors/double-request-eliminator.js'

window.onload = () => {
  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register(
      "https://iot-network.pl/service-worker.js"
    );
  }
};

ReactDOM.render(
  <Router>
    <AlertProvider>
      <UserProvider>
        <LocationsProvider>
          <DevicesProvider>
            <Dashboard />
          </DevicesProvider>
        </LocationsProvider>
      </UserProvider>
    </AlertProvider>
  </Router>,
  document.getElementById("app")
);
