import React, { useContext, useEffect, useState } from "react";
import { CardBig } from "../../CardBig";
import axios from "axios";
import CONFIG from '../../../config.json';
import { SafetyButton } from "../../basic/SafetyButton";
import { AlertContext } from "../../../contexts/AlertContext";
import { DevicesContext } from "../../../contexts/DevicesContext";

export const EnergyMeterActionsPanel = () => {
  const [initialValue, setInitialValue] = useState(0);
  const alert = useContext(AlertContext);
  const { targetDevice } = useContext(DevicesContext);

  useEffect(() => {
    if (!targetDevice.device) {
      return;
    }

    axios
      .get(`${CONFIG.BACKEND_URL}/devices/${targetDevice.device.id}/energy`)
      .then((response) => {
        setInitialValue(response.data.value);
      });
  }, [targetDevice]);

  const changeValue = () => {
    const data = {
      value: initialValue,
    };

    axios
      .post(
        `${CONFIG.BACKEND_URL}/devices/${targetDevice.device.id}/actions/changeValue`,
        data,
        {
          timeout: 3000,
        }
      )
      .then((response) => {
        setInitialValue(parseInt(response.data.value, 10));

        alert.success("Initial value has been set");
      })
      .catch((error) => {
        alert.error(error);
      });
  };

  const handleInputInitialValue = (event) => {
    setInitialValue(parseInt(event.target.value, 10));
  };

  return (
    <CardBig title="Energy Meter Actions Panel">
      <div className="row mt-1 mb-2">
        <div className="col-3">
          <label className="col-form-label">Initial value:</label>
        </div>
        <div className="col-9">
          <div className="input-group mb-2">
            <input
              className="form-control"
              type="number"
              onInput={handleInputInitialValue}
              value={initialValue}
            />
            <div className="input-group-append">
              <div className="input-group-text">Wh</div>
            </div>
          </div>
        </div>
      </div>
      <div className="row mt-1">
        <div className="col text-center">
          <SafetyButton clickCallback={changeValue} name="Save" />
        </div>
      </div>
    </CardBig>
  );
};
