import React, { useContext, useEffect, useState } from "react";
import { Card } from "../../Card";
import axios from "axios";
import CONFIG from '../../../config.json';
import { DevicesContext } from "../../../contexts/DevicesContext";

export const EnergyMeterValueCard = () => {
  const { targetDevice } = useContext(DevicesContext);
  const [energyValue, setEnergyValue] = useState(0);

  useEffect(() => {
    if (!targetDevice.device) {
      return;
    }

    axios
      .get(`${CONFIG.BACKEND_URL}/devices/${targetDevice.device.id}/energy`)
      .then((response) => {
        setEnergyValue(response.data.value);
      });
  }, [targetDevice]);

  return (
    <Card
      title="Zużycie energii elektrycznej:"
      value={`${energyValue / 1000} kWh`}
      icon="fa-bolt"
      color="danger"
    />
  );
};
