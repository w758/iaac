import React, { useContext, useEffect, useState } from "react";
import { Card } from "../../Card";
import PropTypes from "prop-types";
import axios from "axios";
import CONFIG from '../../../config.json';
import { DevicesContext } from "../../../contexts/DevicesContext";

export const HygrostatAndThermostatHumidityCard = (props) => {
  const { targetDevice } = useContext(DevicesContext);
  const [humidity, setHumidity] = useState(0);

  useEffect(() => {
    if (!targetDevice.device && !props.device) {
      return;
    }

    axios
      .get(
        `${CONFIG.BACKEND_URL}/devices/${
          props.device?.id ?? targetDevice.device.id
        }/humidity`
      )
      .then((response) => {
        setHumidity(response.data.value);
      });
  }, [targetDevice]);

  return (
    <Card
      title={`Wilgotność - ${props.device?.name ?? targetDevice.device?.name}`}
      value={`${humidity} %`}
      icon="fa-tint"
      color="info"
    />
  );
};

HygrostatAndThermostatHumidityCard.propTypes = {
  device: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  }),
};
