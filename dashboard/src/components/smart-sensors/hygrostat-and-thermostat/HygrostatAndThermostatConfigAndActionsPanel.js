import React, { useContext, useEffect, useState } from "react";
import { CardBig } from "../../CardBig";
import { Switch } from "../../basic/Switch";
import axios from "axios";
import CONFIG from '../../../config.json';
import { AlertContext } from "../../../contexts/AlertContext";
import { DevicesContext } from "../../../contexts/DevicesContext";

const MODE = {
  MANUAL: "manual",
  THERMOSTAT: "thermostat",
  HYGROSTAT: "hygrostat",
};

const DEVICE_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};

export const HygrostatAndThermostatConfigAndActionsPanel = () => {
  const [mode, setMode] = useState(MODE.MANUAL);
  const [humidityThreshold, setHumidityThreshold] = useState(0);
  const [temperatureThreshold, setTemperatureThreshold] = useState(0);
  const [deviceStatus, setDeviceStatus] = useState(DEVICE_STATUS.INACTIVE);
  const alert = useContext(AlertContext);
  const { targetDevice } = useContext(DevicesContext);

  const getDeviceStatus = () => {
    axios
      .get(`${CONFIG.BACKEND_URL}/devices/${targetDevice.device.id}/status`)
      .then((response) => {
        setDeviceStatus(
          parseInt(response.data.value) === DEVICE_STATUS.INACTIVE
            ? DEVICE_STATUS.INACTIVE
            : DEVICE_STATUS.ACTIVE
        );
      });
  };

  const getMode = () => {
    axios
      .get(`${CONFIG.BACKEND_URL}/devices/${targetDevice.device.id}/mode`)
      .then((response) => {
        setMode(response.data.value);
      });
  };

  useEffect(() => {
    if (!targetDevice.device) {
      return;
    }

    getDeviceStatus();
    getMode();

    axios
      .get(`${CONFIG.BACKEND_URL}/devices/${targetDevice.device.id}/config`)
      .then((response) => {
        setHumidityThreshold(parseFloat(response.data.humidityThreshold));
        setTemperatureThreshold(parseFloat(response.data.temperatureThreshold));
      });
  }, [targetDevice]);

  const handleChangeMode = (event) => {
    const value = event.target.value;

    axios
      .post(
        `${CONFIG.BACKEND_URL}/devices/${targetDevice.device.id}/actions/changeMode`,
        { value }
      )
      .then((response) => {
        setMode(value);

        getDeviceStatus();
      })
      .catch((error) => {
        alert.error(error.message);
      });
  };

  const handleChangeDeviceStatus = (event) => {
    const status =
      parseInt(event.target.value) === DEVICE_STATUS.INACTIVE
        ? DEVICE_STATUS.INACTIVE
        : DEVICE_STATUS.ACTIVE;

    axios
      .post(
        `${CONFIG.BACKEND_URL}/devices/${targetDevice.device.id}/actions/onOff`,
        {
          value: Boolean(status),
        }
      )
      .then((response) => {
        setDeviceStatus(
          parseInt(response.data.value) === DEVICE_STATUS.INACTIVE
            ? DEVICE_STATUS.INACTIVE
            : DEVICE_STATUS.ACTIVE
        );

        alert.info("Status changed.");
      })
      .catch((error) => {
        alert.error(error);
      });
  };

  const saveConfig = () => {
    const data = {
      humidityThreshold,
      temperatureThreshold,
    };

    axios
      .put(
        `${CONFIG.BACKEND_URL}/devices/${targetDevice.device.id}/config`,
        data
      )
      .then((response) => {
        setHumidityThreshold(parseFloat(response.data.humidityThreshold));
        setTemperatureThreshold(parseFloat(response.data.temperatureThreshold));

        alert.success("Saved");

        getDeviceStatus();
        getMode();
      })
      .catch((error) => {
        alert.error(error);
      });
  };

  const handleInputHumidityThreshold = (event) => {
    setHumidityThreshold(parseFloat(event.target.value));
  };

  const handleInputTemperatureThreshold = (event) => {
    setTemperatureThreshold(parseFloat(event.target.value));
  };

  return (
    <CardBig title="Thermostat And Thermostat Config Panel">
      <div className="row">
        <div className="col-sm-4 vertical-center">Fan:</div>
        <div className="col">
          <Switch
            callback={handleChangeDeviceStatus}
            checked={deviceStatus}
            options={[
              {
                name: "active",
                value: DEVICE_STATUS.ACTIVE,
              },
              {
                name: "inactive",
                value: DEVICE_STATUS.INACTIVE,
              },
            ]}
          />
        </div>
      </div>

      <div className="row mt-1">
        <div className="col-sm-4 vertical-center">Mode:</div>
        <div className="col">
          <Switch
            callback={handleChangeMode}
            checked={mode}
            options={[
              {
                name: MODE.MANUAL,
                value: MODE.MANUAL,
              },
              {
                name: MODE.THERMOSTAT,
                value: MODE.THERMOSTAT,
              },
              {
                name: MODE.HYGROSTAT,
                value: MODE.HYGROSTAT,
              },
            ]}
          />
        </div>
      </div>
      <div className="row mt-1">
        <div className="col-sm-4 vertical-center">Humidity threshold:</div>
        <div className="col">
          <input
            className="form-control"
            type="number"
            onChange={handleInputHumidityThreshold}
            value={humidityThreshold}
          />
        </div>
      </div>
      <div className="row mt-1">
        <div className="col-sm-4 vertical-center">Temperature threshold:</div>
        <div className="col">
          <input
            className="form-control"
            type="number"
            onChange={handleInputTemperatureThreshold}
            value={temperatureThreshold}
          />
        </div>
      </div>
      <div className="row mt-1">
        <div className="col text-center">
          <button className="btn btn-primary" onClick={saveConfig}>
            Save
          </button>
        </div>
      </div>
    </CardBig>
  );
};
