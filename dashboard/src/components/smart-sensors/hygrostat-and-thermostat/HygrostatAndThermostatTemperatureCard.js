import React, { useContext, useEffect, useState } from "react";
import { Card } from "../../Card";
import axios from "axios";
import CONFIG from '../../../config.json';
import { DevicesContext } from "../../../contexts/DevicesContext";

export const HygrostatAndThermostatTemperatureCard = () => {
  const { targetDevice } = useContext(DevicesContext);
  const [temperature, setTemperature] = useState(0);

  useEffect(() => {
    if (!targetDevice.device) {
      return;
    }

    axios
      .get(
        `${CONFIG.BACKEND_URL}/devices/${targetDevice.device.id}/temperature`
      )
      .then((response) => {
        setTemperature(response.data.value);
      });
  }, [targetDevice]);

  return (
    <Card
      title={`Temperatura - ${targetDevice.device?.name}`}
      value={`${temperature} °C`}
      icon="fa-thermometer-half"
      color="danger"
    />
  );
};
