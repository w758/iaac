import React, { useEffect, useState } from "react";
import { SketchPicker } from "react-color";
import CONFIG from '../../../config.json';
import axios from "axios";
import { CardBig } from "../../CardBig";
import { Switch } from "../../basic/Switch";

export const LedRgbStripCard = () => {
  const [color, setColor] = useState({
    r: 0,
    g: 0,
    b: 0,
  });
  const [isEnabled, setIsEnabled] = useState(false);
  const [displayColorPicker, setDisplayColorPicker] = useState(false);
  const [effects, setEffects] = useState([]);
  const [selectedEffect, setSelectedEffect] = useState("none");

  useEffect(() => {
    axios.get(`${CONFIG.BACKEND_URL}/led-rgb-strip`).then((response) => {
      setColor({
        r: response.data.color.red,
        g: response.data.color.green,
        b: response.data.color.blue,
      });
    });

    axios
      .get(`${CONFIG.BACKEND_URL}/led-rgb-strip/effects`)
      .then((response) => {
        setEffects(response.data.effects);
      });
  }, []);

  const rgbToHex = (color) => {
    return `#${Number(color.r).toString(16)}${Number(color.g).toString(
      16
    )}${Number(color.b).toString(16)}`;
  };

  const handleChangeStatus = (event) => {
    axios
      .post(`${CONFIG.BACKEND_URL}/led-rgb-strip/switch`, {
        status: event.target.value === "enable",
      })
      .then((response) => {
        // setState({
        //   status: response.data.status
        // })
        // co to jest?
        // chyba do isEnabled parsowanie
      })
      .catch((error) => {
        alert(error.message);
      });
  };

  const handleChangeColor = (color) => {
    const data = {
      red: color.rgb.r,
      green: color.rgb.g,
      blue: color.rgb.b,
    };

    axios
      .put(`${CONFIG.BACKEND_URL}/led-rgb-strip/change-color`, data)
      .then((response) => {
        setColor({
          r: response.data.color.red,
          g: response.data.color.green,
          b: response.data.color.blue,
        });
      })
      .catch((error) => {
        alert(error.message);
      });
  };

  const handleClosePicker = () => {
    setDisplayColorPicker(false);
  };

  const handleToggleColorPicker = () => {
    setDisplayColorPicker(!displayColorPicker);
  };

  const handleSelectEffect = (event) => {
    const data = {
      effect: event.target.value,
    };

    axios
      .put(`${CONFIG.BACKEND_URL}/led-rgb-strip/effects`, data)
      .then((response) => {
        setSelectedEffect(response.data.effect);
      })
      .catch((error) => {
        alert(error.message);
      });
  };

  const styles = {
    colorPickerCover: {
      position: "fixed",
      top: "0px",
      right: "0px",
      bottom: "0px",
      left: "0px",
    },
    colorPickerPopover: {
      position: "absolute",
      zIndex: "2",
    },
    colorPickerButton: {
      border: "2px solid silver",
      background: rgbToHex(color),
    },
  };
  const colorPicker = displayColorPicker ? (
    <div style={styles.colorPickerPopover}>
      <div style={styles.colorPickerCover} onClick={handleClosePicker} />
      <SketchPicker color={color} onChange={handleChangeColor} />
    </div>
  ) : null;

  const options = effects.map((effect) => (
    <option value={effect}>{effect}</option>
  ));

  return (
    <CardBig title="LED RGB Strip">
      <div className="row">
        <div className="col-sm-4 vertical-center">Change status:</div>
        <div className="col vertical-center">
          <Switch
            callback={handleChangeStatus}
            checked={isEnabled}
            options={[
              {
                name: "on",
                value: "enable",
              },
              {
                name: "off",
                value: "disable",
              },
            ]}
          />
        </div>
      </div>

      <div className="row mt-1">
        <div className="col-sm-4 vertical-center">Current color:</div>
        <div className="col vertical-center">
          <button
            className="btn"
            style={styles.colorPickerButton}
            onClick={handleToggleColorPicker}
          >
            color
          </button>
          {colorPicker}
        </div>
      </div>

      <div className="row mt-1">
        <div className="col-sm-4 vertical-center">Effect:</div>
        <div className="col vertical-center">
          <select
            className="form-control"
            onChange={handleSelectEffect}
            value={selectedEffect}
          >
            {options}
          </select>
        </div>
      </div>
    </CardBig>
  );
};
