export const DEVICES_INITIAL_STATE = [];

const actionTypes = {
  FETCH_DEVICES: "FETCH_DEVICES",
  ADD_DEVICE: "ADD_DEVICE",
};

const deviceReducer = (state, action) => {
  switch (action.type) {
    case actionTypes.FETCH_DEVICES:
      return {};
  }
};
