import React from "react";
import Chart from "chart.js";
import PropTypes from "prop-types";
import { mergeDeep } from "../../helpers/MergeDeepHelper";
import DatePicker from "react-datepicker";

export const TwoLineChart = (props) => {
  const ctx = document.getElementById("twoLineChart");

  if (ctx !== null) {
    const options = mergeDeep(
      {
        scales: {
          xAxes: [
            {
              type: "time",
              distribution: "series",
              time: {
                unit: "minute",
              },
            },
          ],
        },
        plugins: {
          zoom: {
            zoom: {
              enabled: true,
              mode: "xy",
            },
            pan: {
              // Boolean to enable panning
              enabled: true,
              mode: "xy",
            },
          },
        },
        spanGaps: true,
      },
      props.options
    );

    new Chart(ctx, {
      type: "line",
      data: {
        labels: props.labels,
        datasets: [
          {
            label: props.labelName,
            fill: false,
            data: props.data,
            borderColor: ["rgba(255, 99, 132, 1)"],
            borderWidth: 1,
            pointRadius: 0,
          },
          {
            label: props.labelName,
            fill: false,
            data: props.secondData,
            borderColor: ["rgba(54, 162, 235, 1)"],
            borderWidth: 1,
            pointRadius: 0,
          },
        ],
      },
      options,
    });
  }

  const datePickerFrom = props.datetimeFrom && (
    <DatePicker
      className="form-control"
      selected={props.datetimeFrom.value}
      onChange={(date) => props.datetimeFrom.callback(date)}
      showTimeSelect
      timeFormat="HH:mm"
      timeIntervals={15}
      timeCaption="time"
      dateFormat="MMMM d, yyyy h:mm aa"
    />
  );

  const datePickerTo = props.datetimeTo && (
    <DatePicker
      className="form-control"
      selected={props.datetimeTo.value}
      onChange={(date) => props.datetimeTo.callback(date)}
      showTimeSelect
      timeFormat="HH:mm"
      timeIntervals={15}
      timeCaption="time"
      dateFormat="MMMM d, yyyy h:mm aa"
    />
  );

  return (
    <div className="card shadow mb-4">
      <div className="card-header py-3">
        <h6 className="m-0 fw-bold text-primary">
          Temperature in my room Chart
        </h6>
      </div>
      <div className="card-body">
        <div className="chart-area">
          <canvas id="twoLineChart"></canvas>
        </div>
        <hr />

        <div className="row">
          <div className="col">From: {datePickerFrom}</div>
          <div className="col">To: {datePickerTo}</div>
        </div>
      </div>
    </div>
  );
};

TwoLineChart.propTypes = {
  labelName: PropTypes.string.isRequired,
  labels: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  secondData: PropTypes.array.isRequired,
  options: PropTypes.object,
  datetimeFrom: PropTypes.exact({
    value: PropTypes.number.isRequired,
    callback: PropTypes.func.isRequired,
  }),
  datetimeTo: PropTypes.exact({
    value: PropTypes.number.isRequired,
    callback: PropTypes.func.isRequired,
  }),
};

TwoLineChart.defaultProps = {
  options: {},
  datetimeFrom: null,
  datetimeTo: null,
};
