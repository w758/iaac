import React from "react";
import { Line } from "react-chartjs-2";
import PropTypes from "prop-types";
import { mergeDeep } from "../../helpers/MergeDeepHelper";
import "chartjs-plugin-zoom";
import "hammerjs";
import DatePicker from "react-datepicker";
import { CardBig } from "../CardBig";
import "chartjs-adapter-moment";

const colors = {
  red: {
    background: "rgba(255, 99, 132, 0.2)",
    border: "rgba(255, 99, 132, 1)",
  },
  green: {
    background: "rgba(67, 177, 119, 0.2)",
    border: "rgba(67, 177, 119, 1)",
  },
  blue: {
    background: "rgba(67, 93, 177, 0.2)",
    border: "rgba(67, 93, 177, 1)",
  },
};

export const LineChart = (props) => {
  const options = mergeDeep(
    {
      scales: {
        x: {
          type: "time",
          // distribution: "series",
          time: {
            displayFormats: {
              millisecond: "H:mm:ss",
              second: "H:mm:ss",
              minute: "H:mm",
              hour: "MM.DD H:mm",
              day: "MM.DD",
              week: "MM.DD",
              month: "MM.DD",
              quarter: "MM.DD",
              year: "YYYY.MM.DD",
            },
          },
        },
      },
      plugins: {
        zoom: {
          zoom: {
            enabled: true,
            mode: "xy",
          },
          pan: {
            // Boolean to enable panning
            enabled: true,
            mode: "xy",
          },
        },
      },
    },
    props.options
  );

  const datePickerFrom = props.datetimeFrom && (
    <DatePicker
      className="form-control"
      selected={props.datetimeFrom.value}
      onChange={(date) => props.datetimeFrom.callback(date)}
      showTimeSelect
      timeFormat="HH:mm"
      timeIntervals={15}
      timeCaption="time"
      dateFormat="MMMM d, yyyy h:mm aa"
    />
  );

  const datePickerTo = props.datetimeTo && (
    <DatePicker
      className="form-control"
      selected={props.datetimeTo.value}
      onChange={(date) => props.datetimeTo.callback(date)}
      showTimeSelect
      timeFormat="HH:mm"
      timeIntervals={15}
      timeCaption="time"
      dateFormat="MMMM d, yyyy h:mm aa"
    />
  );

  const resolutionValues = ["1MIN", "15MIN", "30MIN", "1H", "6H", "12H", "1D"];
  const resolutionOptions = resolutionValues.map((value) => (
    <option key={`option-${value}`} value={value}>
      {value}
    </option>
  ));
  const resolution = (
    <select
      value={props.resolution.value}
      className="form-control"
      onChange={props.resolution.callback}
    >
      {resolutionOptions}
    </select>
  );

  return (
    <CardBig title={`${props.labelName} Chart`}>
      <div className="chart-area">
        <Line
          type="line"
          data={{
            labels: props.labels,
            datasets: [
              {
                label: props.labelName,
                data: props.data,
                backgroundColor: [colors[props.color].background],
                borderColor: [colors[props.color].border],
                borderWidth: 1,
                pointRadius: 0,
              },
            ],
          }}
          options={options}
        />
      </div>
      <hr />
      <div className="row">
        <div className="col">Resolution: {resolution}</div>
        <div className="col">From: {datePickerFrom}</div>
        <div className="col">To: {datePickerTo}</div>
      </div>
    </CardBig>
  );
};

LineChart.propTypes = {
  id: PropTypes.string.isRequired,
  labelName: PropTypes.string.isRequired,
  labels: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  options: PropTypes.object,
  datetimeFrom: PropTypes.exact({
    value: PropTypes.number.isRequired,
    callback: PropTypes.func.isRequired,
  }),
  datetimeTo: PropTypes.exact({
    value: PropTypes.number.isRequired,
    callback: PropTypes.func.isRequired,
  }),
  resolution: PropTypes.exact({
    value: PropTypes.string.isRequired,
    callback: PropTypes.func.isRequired,
  }),
  color: PropTypes.oneOf(["red", "green", "blue"]),
};

LineChart.defaultProps = {
  options: {},
  datetimeFrom: null,
  datetimeTo: null,
  resolution: null,
  color: "red",
};
