import React, { useEffect, useState } from "react";
import moment from "moment";
import CONFIG from '../../config.json';
import { LineChart } from "./LineChart";
import axios from "axios";
import PropTypes from "prop-types";

const DATETIME_FORMAT = "YYYY-MM-DD HH:mm:ss";

export const PropertyChart = (props) => {
  const [values, setValues] = useState([]);
  const [datetimeFrom, setDatetimeFrom] = useState(
    moment().subtract(7, "d").format(DATETIME_FORMAT)
  );
  const [datetimeTo, setDatetimeTo] = useState(
    moment().format(DATETIME_FORMAT)
  );
  const [resolution, setResolution] = useState("1H");

  const convertPayloadToRecord = (payload) => {
    return {
      datetime: moment(payload.createdAt).format(DATETIME_FORMAT),
      value:
        typeof props.mapValue === "function"
          ? props.mapValue(payload.value.value)
          : payload.value.value,
    };
  };

  useEffect(() => {
    axios
      .get(
        `${CONFIG.BACKEND_URL}/devices/${props.deviceId}/${props.property}/history`,
        {
          params: {
            datetimeFrom,
            datetimeTo,
            resolution,
          },
        }
      )
      .then((response) => {
        const records = response.data.map((payload) =>
          convertPayloadToRecord(payload)
        );

        setValues([...records]);
      });
  }, [datetimeFrom, datetimeTo, resolution]);

  const changeDatetimeFromHandler = (date) => {
    setDatetimeFrom(moment(date).format(DATETIME_FORMAT));
  };

  const changeDatetimeToHandler = (date) => {
    setDatetimeTo(moment(date).format(DATETIME_FORMAT));
  };

  const changeResolutionHandler = (event) => {
    setResolution(event.target.value);
  };

  const labels = [];
  const data = [];

  values.forEach((measure) => {
    labels.push(moment(measure.datetime));
    data.push(measure.value);
  });

  return (
    <LineChart
      id={props.id}
      color={props.color ?? "red"}
      labelName={props.labelName}
      labels={labels}
      data={data}
      datetimeFrom={{
        value: moment(datetimeFrom).valueOf(),
        callback: changeDatetimeFromHandler,
      }}
      datetimeTo={{
        value: moment(datetimeTo).valueOf(),
        callback: changeDatetimeToHandler,
      }}
      resolution={{
        value: resolution,
        callback: changeResolutionHandler,
      }}
    />
  );
};

PropertyChart.propTypes = {
  deviceId: PropTypes.string.isRequired,
  property: PropTypes.string.isRequired,
  labelName: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  mapValue: PropTypes.func,
  color: PropTypes.string,
};
