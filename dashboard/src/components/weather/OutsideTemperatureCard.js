import React, {useContext, useEffect, useState} from "react";
import { Card } from "../Card";
import axios from "axios";
import CONFIG from '../../config.json';
import {AlertContext} from "../../contexts/AlertContext";

export const OutsideTemperatureCard = () => {
  const [temperature, setTemperature] = useState(0);
  const { alert } = useContext(AlertContext)

  useEffect(() => {
    axios.get(`${CONFIG.BACKEND_URL}/weather/temperature`).then((response) => {
      setTemperature(response.data.value);
    }).catch(e => alert.error(e));
  }, []);

  return (
    <Card
      title="Temperatura na zewnątrz"
      value={`${temperature} °C`}
      icon="fa-thermometer-half"
      color="danger"
    />
  );
};
