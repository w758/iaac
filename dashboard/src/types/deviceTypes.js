export const DeviceTypes = {
  gasSensor: "gas-sensor",
  hygrostatAndThermostat: "hygrostat-and-thermostat",
  energyMeter: "energy-meter",
  remoteGate: "remote-gate",
  smartPlug: "smart-plug",
};
