import React, { createContext, useState } from "react";
import PropTypes from "prop-types";
import { useLocalStorage } from "react-use";

export const UserContext = createContext();

export const UserProvider = (props) => {
  const [userInMemory, setUserInMemory, removeUserInMemory] = useLocalStorage(
    "user",
    null
  );
  const [user, setUser] = useState(userInMemory);

  const value = {
    user: {
      set: (u) => {
        setUser(u);
        setUserInMemory(u);
      },
      get: () => user,
      isLoggedIn: () => !!user,
      logout: () => {
        removeUserInMemory();
        setUser(null);
      },
    },
  };

  return (
    <UserContext.Provider value={value}>{props.children}</UserContext.Provider>
  );
};

UserProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
