import React, { createContext, useContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import CONFIG from '../config.json';
import { UserContext } from "./UserContext";
import { AlertContext } from "./AlertContext";

export const LocationsContext = createContext();

export const LocationsProvider = (props) => {
  const [locations, setLocations] = React.useState([]);
  const [targetLocation, setTargetLocation] = useState(null);
  const { user } = useContext(UserContext);
  const alert = useContext(AlertContext);
  const value = {
    locations: {
      get: () => locations,
      fetch: () => {
        axios
          .get(`${CONFIG.BACKEND_URL}/locations`)
          .then((response) => {
            setLocations(response.data);
          })
          .catch((e) => {
            alert.error(e);
          });
      },
    },
    targetLocation: {
      location: targetLocation,
      set: (locationId) =>
        setTargetLocation(
          locations.find((location) => location.id === locationId)
        ),
    },
  };

  useEffect(() => {
    if (user.isLoggedIn()) {
      value.locations.fetch();
    }
  }, [user]);

  return (
    <LocationsContext.Provider value={value}>
      {props.children}
    </LocationsContext.Provider>
  );
};

LocationsProvider.propTypes = {
  children: PropTypes.node.isRequired,
  route: PropTypes.any,
};
