import React, { createContext, useContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import CONFIG from '../config.json';
import { UserContext } from "./UserContext";
import { AlertContext } from "./AlertContext";
import { LocationsContext } from "./LocationsContext";

export const DevicesContext = createContext();

export const DevicesProvider = (props) => {
  const [devices, setDevices] = useState([]);
  const [targetDevice, setTargetDevice] = useState(null);
  const { user } = useContext(UserContext);
  const { targetLocation } = useContext(LocationsContext);
  const alert = useContext(AlertContext);
  const value = {
    devices: {
      get: (type = null) =>
        devices.filter(
          (device) =>
            ((type && device.type === type) || !type) &&
            ((targetLocation.location &&
              targetLocation.location.id === device.locationId) ||
              !targetLocation.location)
        ),
      fetch: (type = "") => {
        axios
          .get(`${CONFIG.BACKEND_URL}/devices`, {
            params: {
              type,
            },
          })
          .then((response) => {
            setDevices(response.data);
          })
          .catch((e) => {
            alert.error(e);
          });
      },
    },
    targetDevice: {
      device: targetDevice,
      set: (deviceId) =>
        setTargetDevice(devices.find((device) => device.id === deviceId)),
    },
  };

  useEffect(() => {
    if (user.isLoggedIn()) {
      value.devices.fetch();
    }
  }, [user]);

  return (
    <DevicesContext.Provider value={value}>
      {props.children}
    </DevicesContext.Provider>
  );
};

DevicesProvider.propTypes = {
  children: PropTypes.node.isRequired,
  route: PropTypes.any,
};
