import React, { useContext, useEffect } from "react";
import { useParams } from "react-router-dom";
import { DevicesContext } from "../contexts/DevicesContext";
import { uuidRegex } from "../helpers/Validators";
import { RemoteGateConfigPanel } from "../components/smart-sensors/remote-gate/RemoteGateConfigPanel";

export const RemoteGatePage = () => {
  const { deviceId } = useParams();
  const { targetDevice, devices } = useContext(DevicesContext);

  useEffect(() => {
    targetDevice.set(deviceId);
  }, [deviceId, devices]);

  if (!uuidRegex.test(deviceId)) {
    return <span>Id is invalid</span>;
  }

  return (
    <div className="container-fluid">
      <h1 className="h3 mb-4 text-gray-800">Remote Gate Page</h1>
      <div className="row">
        <div className="col-xl-6 col-lg-6">
          <RemoteGateConfigPanel />
        </div>
      </div>
    </div>
  );
};
