import React, { useContext, useEffect } from "react";
import { HygrostatAndThermostatHumidityCard } from "../components/smart-sensors/hygrostat-and-thermostat/HygrostatAndThermostatHumidityCard";
import { HygrostatAndThermostatConfigAndActionsPanel } from "../components/smart-sensors/hygrostat-and-thermostat/HygrostatAndThermostatConfigAndActionsPanel";
import { HygrostatAndThermostatTemperatureCard } from "../components/smart-sensors/hygrostat-and-thermostat/HygrostatAndThermostatTemperatureCard";
import { useParams } from "react-router-dom";
import { DevicesContext } from "../contexts/DevicesContext";
import { uuidRegex } from "../helpers/Validators";
import { PropertyChart } from "../components/basic/PropertyChart";

export const HygrostatAndThermostatPage = () => {
  const { deviceId } = useParams();
  const { targetDevice, devices } = useContext(DevicesContext);

  useEffect(() => {
    targetDevice.set(deviceId);
  }, [deviceId, devices]);

  if (!uuidRegex.test(deviceId)) {
    return <span>Id is invalid</span>;
  }

  return (
    <div className="container-fluid">
      <h1 className="h3 mb-4 text-gray-800">Hygrostat & Thermostat Page</h1>
      <div className="row">
        <div className="col-xl-3 col-md-6 mb-4">
          <HygrostatAndThermostatTemperatureCard />
        </div>
        <div className="col-xl-3 col-md-6 mb-4">
          <HygrostatAndThermostatHumidityCard />
        </div>
        <div className="col-xl-6 col-lg-6">
          <HygrostatAndThermostatConfigAndActionsPanel />
        </div>
      </div>
      <div className="row">
        <div className="col-xl-6 col-lg-6">
          <PropertyChart
            deviceId={deviceId}
            property="humidity"
            labelName="Humidity"
            id="hygrostatAndThermostatHumidity"
            mapValue={(value) => parseFloat(value)}
            color="blue"
          />
        </div>
        <div className="col-xl-6 col-lg-6">
          <PropertyChart
            deviceId={deviceId}
            property="temperature"
            labelName="Temperature"
            id="hygrostatAndThermostatTemperature"
            mapValue={(value) => parseFloat(value)}
          />
        </div>
      </div>
    </div>
  );
};
