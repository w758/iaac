import React, { useContext, useEffect } from "react";
import { useParams } from "react-router-dom";
import { DevicesContext } from "../contexts/DevicesContext";
import { uuidRegex } from "../helpers/Validators";
import { GasSensorCard } from "../components/smart-sensors/gas-sensor/GasSensorCard";

export const GasSensorPage = () => {
  const { deviceId } = useParams();
  const { targetDevice, devices } = useContext(DevicesContext);

  useEffect(() => {
    targetDevice.set(deviceId);
  }, [deviceId, devices]);

  if (!uuidRegex.test(deviceId)) {
    return <span>Id is invalid</span>;
  }

  return (
    <div className="container-fluid">
      <h1 className="h3 mb-4 text-gray-800">Gas Sensor Page</h1>
      <div className="row">
        <div className="col-xl-3 col-md-6 mb-4">
          <GasSensorCard />
        </div>
      </div>
    </div>
  );
};
