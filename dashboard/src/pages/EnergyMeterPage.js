import React, { useContext, useEffect } from "react";
import { EnergyMeterValueCard } from "../components/smart-sensors/energy-meter/EnergyMeterValueCard";
import { EnergyMeterActionsPanel } from "../components/smart-sensors/energy-meter/EnergyMeterActionsPanel";
import { PropertyChart } from "../components/basic/PropertyChart";
import { useParams } from "react-router-dom";
import { DevicesContext } from "../contexts/DevicesContext";
import { uuidRegex } from "../helpers/Validators";

export const EnergyMeterPage = () => {
  const { deviceId } = useParams();
  const { targetDevice, devices } = useContext(DevicesContext);

  useEffect(() => {
    targetDevice.set(deviceId);
  }, [deviceId, devices]);

  if (!uuidRegex.test(deviceId)) {
    return <span>Id is invalid</span>;
  }

  return (
    <div className="container-fluid">
      <h1 className="h3 mb-4 text-gray-800">Energy Meter Page</h1>
      <div className="row">
        <div className="col-xl-3 col-md-6 mb-4">
          <EnergyMeterValueCard />
        </div>
        <div className="col-xl-6 col-lg-6">
          <EnergyMeterActionsPanel />
        </div>
      </div>
      <div className="row">
        <div className="col-xl-6 col-lg-6">
          <PropertyChart
            deviceId={deviceId}
            property="energy"
            labelName="Energy"
            id="energyChart"
            mapValue={(value) => (parseFloat(value) / 1000).toFixed(3)}
          />
        </div>
      </div>
    </div>
  );
};
